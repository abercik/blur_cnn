#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  3 19:19:55 2018

@author: verdu
"""

from numpy import abs, fft, zeros,shape,copy,save
from cv2 import imread
#import glob
from matplotlib.pyplot import figure,imshow,close,tick_params
from random import randint
import scipy.ndimage as img
from os import listdir

def makePSF(length,width,theta,array):
    """ Returns a linear point spread function 
    INPUT: length of x and y blurs, blurry image (to copy shape of array)
    OUTPUT: array of same size as input that defines PSF """   
    # initialize gaussian array of same size as input array
    psf = zeros(shape(array))
    rows = len(psf[0]) # store dimensions
    cols = len(psf)
    
    psf[cols//2-width//2:cols//2+width//2,rows//2:rows//2+length] = 1. # make linear psf
    psf = img.rotate(psf, theta, reshape=False) # rotate by theta
   
    # transform to corners because of the weird fft indexing
    topleft = copy(psf[0:cols//2,0:rows//2])
    topright = copy(psf[0:cols//2,rows//2:rows])
    bottleft = copy(psf[cols//2:cols,0:rows//2])
    bottright = copy(psf[cols//2:cols,rows//2:rows])
    
    psf[0:cols//2,0:rows//2],psf[0:cols//2,rows//2:rows],psf[cols//2:cols,
            0:rows//2],psf[cols//2:cols,rows//2:rows] = bottright,bottleft,topright,topleft
    
    return psf

def convolve(image, psf):
    """ Convolves the images with a given point spread function"""
    K = len(psf[0])
    L = len(psf)
    image_fft = fft.fft2(image) # fourier transform image
    psf_fft = fft.fft2(psf) # fourier transform point spread function 
    convolved_fft = image_fft*psf_fft*K*L
    convolved = abs(fft.ifft2(convolved_fft))
    return convolved # return deconvolved image

# now loop through images in original folder and make new blurry images
#path.append('/original')
filelist = listdir('original') # load all the filenames
# sometimes this happens - so remove this hidden file just in case
if '.DS_Store' in filelist: filelist.remove('.DS_Store')
for filename in filelist: #assuming all iteams in directory are jpg
    image = imread('original/' + filename,0) # read as grayscale
    for i in range(5): # do it 5 times for each image
        # generate randum number for length width and theta
        length = randint(4,20) # keep it small to keep it realistic
        width = 4
        theta = randint(0,89) # can be at any angle
        # convolve image
        psf = makePSF(length,width,theta,image) # make PSF
        convolved = convolve(image,psf) # make blurry image
        # save the image as a binary file to minimize storage size
        save('convolved/{0}_{1}_{2}'.format(filename[:-4],length,theta),convolved)
    
print(' All Done! ')
