import tensorflow
import keras
#from tensorflow import keras # depending on how keras is inported, either this line or the above is used
import os
import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage as img
from cv2 import imread
import argparse 

parser = argparse.ArgumentParser(description='deconvolve images')
parser.add_argument('image', help='image file, ex. 100023_10_45.npy')                
args = parser.parse_args()
file=args.image

# load image
image = np.load('convolved/'+file).reshape(106,106)
image /= np.max(image) # normalize image
# save labels as integers
true_rot = int(file[:-4].split('_')[-1]) # rotation label
true_len = int(file[:-4].split('_')[-2]) # length label

plt.nipy_spectral()

#load mean image
mean_img = np.load('mean_img.npy').reshape(106,106,1)

# load our models
rotmodel = keras.models.load_model('models/rotation_new4.hdf5')
lenmodel = keras.models.load_model('models/length_new_3.hdf5')

def makePSF(length,width,theta,array):
    """ Returns a linear point spread function 
    INPUT: length of x and y blurs, blurry image (to copy shape of array)
    OUTPUT: array of same size as input that defines PSF """   
    # initialize gaussian array of same size as input array
    psf = np.zeros(np.shape(array))
    rows = len(psf[0]) # store dimensions
    cols = len(psf)
    
    psf[cols//2-width//2:cols//2+width//2,rows//2:rows//2+length] = 1. # make linear psf
    psf = img.rotate(psf, theta, reshape=False) # rotate by theta
   
    # transform to corners because of the weird fft indexing
    topleft = np.copy(psf[0:cols//2,0:rows//2])
    topright = np.copy(psf[0:cols//2,rows//2:rows])
    bottleft = np.copy(psf[cols//2:cols,0:rows//2])
    bottright = np.copy(psf[cols//2:cols,rows//2:rows])
    
    psf[0:cols//2,0:rows//2],psf[0:cols//2,rows//2:rows],psf[cols//2:cols,
            0:rows//2],psf[cols//2:cols,rows//2:rows] = bottright,bottleft,topright,topleft
    
    return psf


def deconvolve(Input, psf, epsilon):
  """ Returns the deconvolved (un-blurred) photograph""" 
  InputFFT = np.fft.fft2(Input) # fourier transform blurry image
  psfFFT = np.fft.fft2(psf)+epsilon # fourier transform point spread function
  deconvolved = abs(np.fft.ifft2(InputFFT/psfFFT)) # inverse fourier transform deconvolved image
  return deconvolved

def unblur(image,original=np.array(False)):
    ''' Function that does it all - given a blurry image, run it through the CNN
    to get an output, and deconvolve using the guess PSF. Plot the result.'''
    mean = mean_img.reshape(106,106)
    # make predictions and scale to original values
    rot_pred = int(rotmodel.predict((image-mean).reshape(1,106,106,1))*89)
    len_pred = int(lenmodel.predict((image-mean).reshape(1,106,106,1))*16)+4
    print('Trying to deconvolve image. \n',
          'Guessing PSF Angle = ', rot_pred, '\n',
          'Guessing PSF Length = ', len_pred)
    
    # make the PSF and deconvolve the image
    psf = makePSF(len_pred,4,rot_pred,image)
    deconvolved = deconvolve(image,psf,0.001)
    
    fig = plt.figure(figsize=(10,4))
    ax1 = fig.add_subplot(1,3,1)
    # Plot the deconvolved image
    ax1.imshow(deconvolved)
    ax1.set_title('Deconvolved (un-blurred) Image')
    ax1.set_ylabel('Y Pixels')
    ax1.set_xlabel('X Pixels \n Guessing PSF Angle = ' + str(rot_pred) +
                   '\n Guessing PSF Length = ' + str(len_pred))
    # Plot the convolved image
    ax2 = fig.add_subplot(1,3,2)
    ax2.imshow(image)
    ax2.set_title('Convolved (blurry) Image')
    ax2.set_xlabel('X Pixels \n True PSF Angle = ' + str(true_rot) +
                   '\n True PSF Length = ' + str(true_len))
    if original.any(): #Plot the original image only if given
        ax3 = fig.add_subplot(1,3,3)
        ax3.imshow(original)
        ax3.set_title('Original Image')
        ax3.set_xlabel('X Pixels')
    plt.tight_layout()
    plt.show()

#########################################
# Note: Only call unblur with original if you are sure that is the correct image!
filenum = file[:-4].split('_')[0] # first number in file string
original = imread('original/'+filenum+'.jpg',0) # read original (unblurred) image

unblur(image,original)
print('Actual Angle and Length = ', true_rot, true_len)
