# TensorFlow and keras
import tensorflow
import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import os

def load_dataset(path = 'convolved', prcnt_test = 0.2):
    ''' Inport data set, split into training and validation sets
    INPUTS: path where (binary) images are stored, train/test split #
    OUTPUTS: array of flattened train images, train labels list,
             array of flattened test images, test labels list'''
    
    filelist = os.listdir(path) # load all the filenames
    # sometimes this happens - so remove this hidden file just in case
    if '.DS_Store' in filelist: filelist.remove('.DS_Store')
    num_imgs = len(filelist) # store number of images
    
    # pre-allocate memory to store our image data, each image 424x424
    # 20% of images as test images
    test_num = int(num_imgs*prcnt_test)
    test_imgs = np.empty((test_num,106,106,1))
    test_lbls = np.empty(test_num)
    # remainder (80%) of images as train images
    trainnum = num_imgs-int(num_imgs*prcnt_test)
    train_imgs = np.empty((trainnum,106,106,1))
    train_lbls = np.empty(trainnum)
    
    # load images. loop for first 80% of images
    for i, file in enumerate(filelist[:trainnum]):
        # save as entry in test_img array
        train_imgs[i] = np.load(path+'/'+file).reshape(106,106,1)
        train_imgs[i] /= np.max(train_imgs[i]) # normalize image
        # save labels as integers
        train_lbls[i] = int(file[:-4].split('_')[-2])
    # loop for last 20% of images
    for i, file in enumerate(filelist[trainnum:]):
        # save as entry in test_img array
        test_imgs[i] = np.load(path+'/'+file).reshape(106,106,1)
        test_imgs[i] /= np.max(test_imgs[i]) # normalize image
        # save labels as integers
        test_lbls[i] = int(file[:-4].split('_')[-2])

    # return image arrays and lables resized to be scales between 0 and 1                                 
    return train_imgs, ((train_lbls-4)/16), test_imgs, ((test_lbls-4)/16)

# Load the data set
train_imgs, train_lbls, test_imgs, test_lbls = load_dataset()

# Before we do anything, we want to centre the data
mean_img = np.average(train_imgs, axis=0)
plt.imshow(mean_img.reshape(106,106))
np.save('length_new_mean_img',mean_img.reshape(106,106))

######################################################################
# Begin Convolutional Neural Network

model_name = 'length_new_3'

# number of convolutional filters to use
filters = 64
# size of pooling area for max pooling
pool_size = (2, 2)
# convolution kernel size
kernel_size = (4, 4)

train_samples, img_rows, img_cols, img_channels = np.shape(train_imgs)
input_shape = (img_rows, img_cols, img_channels)
test_samples = np.shape(test_imgs)[0]

print('Input shape:', input_shape)
print(train_samples, 'train samples')
print(test_samples, 'test samples')

# model definition
input = keras.layers.Input(shape=(input_shape))
x = keras.layers.Conv2D(filters, kernel_size, activation='tanh')(input)
x = keras.layers.Conv2D(filters, kernel_size, activation='tanh')(x)
x = keras.layers.MaxPooling2D(pool_size=(2, 2))(x)
x = keras.layers.Dropout(0.25)(x)
x = keras.layers.Flatten()(x)
x = keras.layers.Dense(128, activation='tanh')(x)
x = keras.layers.Dropout(0.25)(x)
x = keras.layers.Dense(1, activation='sigmoid')(x)

model = keras.models.Model(inputs=input, outputs=x)
model.summary()

# model compilation
model.compile(loss='mean_squared_error', optimizer='adam',metrics=['mae'])
# Optimizer Option 1: Stochastic gradient descent
# sgd = keras.optimizers.SGD(lr=0.01, decay=0.0, momentum=0.0, nesterov=True)
#       lr = learning rate, decay = amount by which lr decreases on each step, 
#       momentum = pushes SGD in current direction, dampening oscillations
# Optimizer Option 2: Adam (parameters more complicated. Use default)
# loss (loss function): We use simple mean squared error

# training parameters
batch = 128
epoch = 50

# where to save weights
output_folder = 'models'
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

# callbacks
checkpointer = keras.callbacks.ModelCheckpoint(
    filepath=os.path.join(output_folder, model_name + '.hdf5'),
    save_best_only=True) # saving weights
# stop the model early if no longer converging
early_stopping = keras.callbacks.EarlyStopping(patience=4)
# save log files using tensorboard
tensorboard = keras.callbacks.TensorBoard()
# save history in history object
history = keras.callbacks.History()

# training loop
model.fit((train_imgs-mean_img), train_lbls, batch_size=batch,
          epochs=epoch, verbose=0, validation_split = 0.2,
          callbacks=[checkpointer, early_stopping, tensorboard, history])

print('###########################################')
print('Training Complete')
print('###########################################')

score = model.evaluate((test_imgs-mean_img), test_lbls, verbose=0)
print('Test loss:', score[0])
print('Test Mean Abs Error:', score[1]*16)

predictions = model.predict(test_imgs-mean_img).reshape(np.shape(test_lbls))

a= np.average(np.abs(predictions*16-test_lbls*16))
print('Average Length Difference in test set: ', a)

b= np.max(predictions*16-test_lbls*16)
print('Maximum Length Difference in test set: ', b)

# summarize history for accuracy
plt.figure(figsize=(10,6))
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Average Model Loss During Training',fontsize=16)
plt.ylabel('Loss (Mean Squared Error)',fontsize=14)
plt.xlabel('Epoch',fontsize=14)
plt.legend(['train', 'validate'], loc='upper right',fontsize=14)
plt.savefig(model_name + '_loss.pdf')

# summarize history for loss
plt.figure(figsize=(10,6))
plt.plot(np.array(history.history['mean_absolute_error'])*16)
plt.plot(np.array(history.history['val_mean_absolute_error'])*16)
plt.title('Average Length Difference During Training',fontsize=16)
plt.ylabel('Length (Pixel) Difference (Mean Abs Error)',fontsize=14)
plt.xlabel('Epoch',fontsize=14)
plt.legend(['train', 'validate'], loc='upper right',fontsize=14)
plt.savefig( model_name + '_length.pdf')

