# A Deep Convolutional Neural Network used to automatically unblur images.

There are 2 CNN's used, LengthCNN.py and RotationCNN.py They use regression to estimate the angle and length of a linear point spread function that is convolved with images to blur them.

convolvescript.py is a script used to convolve a set of images. I use the data set from Sloan Digital Sky Survey, which gives 60,000 images of 424x424 galaxies. I first downsize the images using the bash script resize.sh in original/, then convolve and set to grayscale using convolvescript.py. The images are saved as binary files in convolved/.

We can then use the python scripts RotationCNN.py and LengthCNN.py to train two neural networks, which will save their models in the models folder.

Once we have this, we can use either the jupyter notebook Predictions.ipynb or the script unblur.py to load the models, make predictions on a blurry image, and plot a deconvolved image.

For more details, see the write-up I made for this project, Report.pdf.

Note: Some sample images are included in this repo, but not all (obviously - would be way too many)

